var express = require('express');
var kafka = require('kafka-node');
var request = require('request');
var app = express();

var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
const kafkaClientOptions = { kafkaHost: 'kafka:9092'};




var topic = function(){
    return new Promise(function (resolve, reject) {
      var client1 = new kafka.KafkaClient(kafkaClientOptions);


      var topicsToCreate = [{
        topic: 'fire',
        partitions: 1,
        replicationFactor: 1
      }];

      client1.createTopics(topicsToCreate, (error, result) => {
        console.log("topic creato");
        // result is an array of any errors if a given topic could not be created
        if(error)
          reject(0);
	else 
          resolve(1);
      });
    });
};

async function createt(){
  let top = await topic();
  if(top === 1){
	var Consumer = kafka.Consumer,
	    client = new kafka.KafkaClient(kafkaClientOptions),
	    consumer = new Consumer(client,
		[{ topic: 'fire', offset: 1}],
		{
		    autoCommit: false
		}
	    );

	consumer.on('message', function (message) {
	    console.log(message);

	    request.post('http://datasaver.decision:3000', {
		json: JSON.parse(message.value)
	    }, (error, res, body) => {
		if (error) {
		    console.error(error)
		    return
		}
		console.log(`statusCode: ${res.statusCode}`)
		console.log(body)
	    });

	});

	consumer.on('error', function (err) {
	    console.log('Error:',err);
	})

	consumer.on('offsetOutOfRange', function (err) {
	    console.log('offsetOutOfRange:',err);
	})
  }
}

createt();

app.get('/',function(req,res){
    res.json({greeting:'Kafka Producer'})
});
/*
app.post('/sendMsg',function(req,res){
    var sentMessage = JSON.stringify(req.body.message);
    payloads = [
        { topic: req.body.topic, messages:sentMessage , partition: 0 }
    ];
    producer.send(payloads, function (err, data) {
        res.json(data);
    });

})
*/


app.listen(5001,function(){
    console.log('Kafka producer running at 5001')
})
