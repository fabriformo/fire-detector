package com.docker;

import java.util.LinkedList;

public class WeightedGraph {
	int N;
    int dim;

    int[][] matrix;
    Coordinates [] coordArray;

    public WeightedGraph(int n, double lat, double lon)
    {
        this.N = n;
        this.dim = n*n;

        matrix = new int[dim][dim];

        //generate adjacency matrix
        for(int i =0; i < dim; i++) {
            for(int j=0; j<dim; j++) {
                if(i==j) matrix[i][j]=0;
                else {
                    if(Math.abs(i-j)==1)
                        if(i%N==0 && j==i-1) matrix[i][j]=0;
                        else if(j%N==0 && i==j-1) matrix[i][j]=0;
                        else                        matrix[i][j]=1;
                    else if(Math.abs(i-j)==N) matrix[i][j]=1;
                    else matrix[i][j]=0;
                }
            }
        }
        
        coordArray = new Coordinates[dim];
        
        for (int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                coordArray[j+N*i] = new Coordinates(Math.round((lat-((double)i*0.0001))*10000000.0) / 10000000.0, Math.round((lon+((double)j*0.0001))*10000000.0) / 10000000.0);
            }
        }

    }
    
    public Coordinates[] getCoordArray() {
		return coordArray;
	}

	public int[][] getMatrix()
    {
        return this.matrix;
    }

    public int getDim() {
        return dim;
    }

    public int getN() {
        return N;
    }

    public void printMatrix()
    {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j <  matrix[i].length; j++) {
                System.out.print( matrix[i][j] + "," + " ");
            }
            System.out.println();
        }
    }

}
