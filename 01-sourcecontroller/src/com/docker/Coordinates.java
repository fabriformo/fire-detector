package com.docker;

public class Coordinates {
	double latitude;
	double longitude;
	
	public Coordinates(double lat, double lon) 
	{
		this.latitude = lat;
		this.longitude = lon;
	}
	
	public double getLatitude()
	{
		return this.latitude;
	}
	
	public double getLongitude()
	{
		return this.longitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	
}
