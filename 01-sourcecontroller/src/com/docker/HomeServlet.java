package com.docker;

import java.io.IOException;
import java.io.PrintWriter;
//import java.io.UnsupportedEncodingException;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//DNS address
	String address = "sensor";
	int port = 3000;
	
	//Parco dei Nebrodi latitude and longitude
    double latitude = 37.9477195;
    double longitude = 14.6697063;
    
    //Temperature and humidity variables
	double min_temp = 0;
    double max_temp = 0;
    double exp_hum = 0;
    
    //Env variable sets the number of vertices
    int propagation = Integer.parseInt(System.getenv("PROPAGATION_ENV"));
	int grid = Integer.parseInt(System.getenv("GRID_ENV"));
    //int propagation = 5000;
	//int grid = 3;
    
    WeightedGraph graph;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
		
		//Graph inizialization
        graph = new WeightedGraph(grid, latitude, longitude);

        //Get current wather from OpenWeatherMap
        OWM owm = new OWM("6064596adb1d6263d1853e1c33dc30c5");
        CurrentWeather cwd = new CurrentWeather();
        try {
			cwd = owm.currentWeatherByCoords(latitude, longitude);
		} catch (APIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        min_temp = Math.round( k2Celsius(cwd.getMainData().getTempMin()) * 100.0) / 100.0;
        max_temp = Math.round( k2Celsius(cwd.getMainData().getTempMax()) * 100.0) / 100.0;
        exp_hum = cwd.getMainData().getHumidity();
        
        //Send coordinates and state to sensors
		for(int i = 0;i < graph.getDim(); i++) {
			try {
				sendPutStartRequest(i, address, graph.getCoordArray()[i].getLatitude(), graph.getCoordArray()[i].getLongitude());
				System.out.println(i);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}						
		}
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        
		request.setAttribute("graph", graph);
        request.setAttribute("min_temp", min_temp);
        request.setAttribute("max_temp", max_temp);
        request.setAttribute("hum", exp_hum);
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}
	
    public static double k2Celsius(double d){
        return (d-273.15);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

			
		PrintWriter out = response.getWriter();
		
		//Creation matrix for nodes visit
		int[][] fire= new int[graph.getDim()][graph.getDim()];

        for(int i=0; i<graph.getDim(); i++) {
            fire[i]= dijkstra(graph.getMatrix(), i, graph.getDim());
        }

        for (int i = 0; i < fire.length; i++) {
            for (int j = 0; j < fire[i].length; j++) {
                System.out.print(fire[i][j] + "," + " ");
            }
            System.out.println();
        }
        System.out.println("--------------------------------------");

        
        boolean warm = false;
        int random_n = (int)(Math.random()*graph.getDim());

        //Fire trigger
        for(int j=0; j<graph.getDim();) {
            for(int i=0; i<graph.getDim(); i++) {
                if(fire[random_n][i]==j && !warm) {
                    System.out.print(i + " - ");
                    sendPutFireRequest(i, address,"warm");
                }
                else if(fire[random_n][i]==j && warm)
                {
                    System.out.print(i + " - ");
                    sendPutFireRequest(i, address,"hot");
                }
            }
            if(!warm) {
                //System.out.print("WARM");
                warm = true;
            }
            else if(warm) {
                //System.out.print("HOT");
                warm = false;
                j++;
            }
            try {
				Thread.sleep(propagation);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            System.out.println("----------------------");
        }
	}
	
	public void sendPutStartRequest(int index, String addr, double lat, double lon) throws UnsupportedEncodingException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut putRequest = new HttpPut("http://"+addr+"-"+index+"."+addr+":"+port+"/start");
        
        String json = "{\"id\":"+(index)+","
        				+"\"max_temp\":"+max_temp+","
        				+"\"min_temp\":"+min_temp+","
        				+"\"exp_hum\":"+exp_hum+","
        				+"\"lat\":"+lat+","
        				+"\"lon\":"+lon+","
        				+ "\"state\":\"cold\""
        				+ "}";
        System.out.println(json);
        putRequest.setEntity(new StringEntity(json));
        putRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        
        try (CloseableHttpResponse httpResponse = httpClient.execute(putRequest)) {
            String content = EntityUtils.toString(httpResponse.getEntity());

            int statusCode = httpResponse.getStatusLine().getStatusCode();
            System.out.println("statusCode = " + statusCode);
            System.out.println("content = " + content);
        } catch (IOException e) {
            //handle exception
            e.printStackTrace();
        }
	}
	
	public void sendPutFireRequest(int index, String addr, String state) throws UnsupportedEncodingException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut putRequest = new HttpPut("http://"+addr+"-"+index+"."+addr+":"+port+"/state");
        String json = "{\"state\":\""+state
        				+"\""
        				+",\"id\":"+index
        				+"}";
        System.out.println(json);
        putRequest.setEntity(new StringEntity(json));
        putRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        
        try (CloseableHttpResponse httpResponse = httpClient.execute(putRequest)) {
            String content = EntityUtils.toString(httpResponse.getEntity());

            int statusCode = httpResponse.getStatusLine().getStatusCode();
            System.out.println("statusCode = " + statusCode);
            System.out.println("content = " + content);
        } catch (IOException e) {
            //handle exception
            e.printStackTrace();
        }
	}
	
	public static int minDistance(int dist[], Boolean sptSet[], int dim) {
        int min = Integer.MAX_VALUE, min_index = -1;

        for (int v = 0; v < dim; v++)
            if (sptSet[v] == false && dist[v] <= min) {
                min = dist[v];
                min_index = v;
            }

        return min_index;
    }
	
	public static int[] dijkstra(int graph[][], int src, int dim)
    {

        Boolean sptSet[] = new Boolean[dim];
        int[] dist = new int[dim];

        for (int i = 0; i < dim; i++)
        {
            dist[i] = Integer.MAX_VALUE;
            sptSet[i] = false;
        }

        dist[src] = 0;

        for (int count = 0; count < dim-1; count++)
        {
            int u = minDistance(dist, sptSet, dim);

            sptSet[u] = true;

            for (int v = 0; v < dim; v++)

                if (!sptSet[v] && graph[u][v]!=0 &&
                        dist[u] != Integer.MAX_VALUE &&
                        dist[u]+graph[u][v] < dist[v])
                    dist[v] = dist[u] + graph[u][v];
        }

        return dist;
    }
	 
}
