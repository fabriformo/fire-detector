<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<link rel="stylesheet" href="bootstrap.css"/>

<script src="https://unpkg.com/ionicons@4.5.5/dist/ionicons.js"></script>

<script type="text/javascript">
function change(){
		document.getElementById("button1").innerHTML = "Please, wait..."
		document.getElementById("button1").disabled = true;
}
</script>

<title>Source Controller</title>

</head>
<body>
	
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	  <a class="navbar-brand" href="#">SourceController <!-- <%=session.getAttribute("random")%> --></a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	</nav>

	<br><br>
	
	
	<div class="container">
	<div class="row justify-content-around">
		<div class="card text-white bg-primary mb-3" style="width: 30rem;" class="col-sm">
		  	<div class="card-body">
		  			<div class="col text-center">
			    		<h4 class="card-title">Temperature and Humidity</h4>
			    	</div>
		    		<p class="card-text"><ion-icon name="thermometer"></ion-icon>Max: ${max_temp}, Min: ${min_temp}</p>
		    		<p class="card-text"><ion-icon name="water"></ion-icon>Expected Humidity: ${hum}%</p>
		    		
		    		<div class="col text-center">
		    			<!-- <button id="button1" class="btn btn-outline-danger" onclick="change()">PROVA</button> -->
		    			<form action="${pageContext.request.contextPath}/" method="post" onsubmit="change()">
		    				<button id="button1" type="submit" class="btn btn-danger">Fire Trigger</button>
		    			</form>    				
		    		</div>
		    </div>
		  </div>
		</div>
	</div>

	<br>
	
	<div class="container">
		<div class="row justify-content-around">
			<c:forEach var="edge" items="${graph.getCoordArray()}" varStatus="loop">
				<div class="card border-success mb-3" style="width: 18rem;" class="col-sm">
		  			<div class="card-body">
		  				<div class="col text-center">
		    				<h4 class="card-title"><b>Sensor ${loop.index}</b></h4>
		    				<p class="card-text"><ion-icon name="locate"></ion-icon> ${edge.getLatitude()}, ${edge.getLongitude()} </p>
		    				<br>
		    			</div>
		    		</div>
	  			</div>
			</c:forEach>
		</div>
	</div>
	
</body>
</html>