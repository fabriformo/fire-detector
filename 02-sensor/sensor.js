const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const kafka = require('kafka-node');

function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var sensor = {
    id: 0,
    lat : 0 ,
    lon : 0 ,
    temperature : 0,
    humidity : 0
}
var initialize = {
    id : 0,
    lat : 0,
    lon : 0,
    min_temp: 0,
    max_temp: 0,
    exp_hum: 0,
    state : 'cold'
};

const kafkaClientOptions = { kafkaHost: 'kafka.communication:9092'};

let start = 0;
let periodo = parseInt(process.env.PERIOD_ENV, 10);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));

/*
var client1 = new kafka.KafkaClient(kafkaClientOptions);


var topicsToCreate = [{
    topic: 'fire',
    partitions: 1,
    replicationFactor: 1
}];

client1.createTopics(topicsToCreate, (error, result) => {

    // result is an array of any errors if a given topic could not be created
});
*/


var Producer = kafka.Producer,
    client = new kafka.KafkaClient(kafkaClientOptions),
    producer = new Producer(client);

producer.on('ready', function () {
    console.log('Producer is ready');
});

producer.on('error', function (err) {
    console.log('Producer is in error state');
    console.log(err);
})
producer.on('error', function (err) {
    console.log('Producer is in error state');
    console.log(err);
})


app.put('/start', (req,res) => {
    initialize = req.body;
    initialize.exp_hum = initialize.exp_hum/100;
    if (req.body.state === 'cold') {
        sensor.temperature = getRandom(initialize.min_temp, initialize.max_temp + 15);
        sensor.humidity = getRandom(initialize.exp_hum, initialize.exp_hum * 0.8);
    } else if (req.body.state === 'warm') {
        sensor.temperature = getRandom(initialize.min_temp + 15, initialize.max_temp + 40);
        sensor.humidity = getRandom(initialize.exp_hum * 0.5, initialize.exp_hum * 0.8);
    } else if (req.body.state === 'hot') {
        sensor.temperature = getRandom(initialize.min_temp + 40, initialize.max_temp + 500);
        sensor.humidity = getRandom(0, initialize.exp_hum * 0.5);
    }
    sensor.lat = initialize.lat;
    sensor.lon = initialize.lon;
    sensor.id = initialize.id;
    console.dir(sensor);
    if (start === 0) {
        setInterval(() => {
            start = 1;
            payloads = [
                {
                    topic: "fire", messages: ['{' +
                    '"id":' +
                    '"' +
                    sensor.id +
                    '",' +
                    '"temp":' +
                    '"' +
                    sensor.temperature +
                    '",' +
                    '"hum":' +
                    '"' +
                    sensor.humidity +
                    '",' +
                    '"lat":' +
                    '' +
                    sensor.lat +
                    ',' +
                    '"lon":' +
                    '' +
                    sensor.lon +
                    '' +
                    '}'], partition: 0
                }
            ];
            console.log(payloads);
            producer.send(payloads, function (err, data) {
                //res.json(data);
            });
        }, periodo);

    }
    res.send(new Buffer('OK'));
});

app.put('/state', (req,res) =>{

    if (req.body.state === 'cold') {
        sensor.temperature = getRandom(initialize.min_temp, initialize.max_temp + 15);
        sensor.humidity = getRandom(initialize.exp_hum, initialize.exp_hum * 0.8);
    } else if (req.body.state === 'warm') {
        sensor.temperature = getRandom(initialize.min_temp + 15, initialize.max_temp + 40);
        sensor.humidity = getRandom(initialize.exp_hum * 0.5, initialize.exp_hum * 0.8);
    } else if (req.body.state === 'hot') {
        sensor.temperature = getRandom(initialize.min_temp + 40, initialize.max_temp + 500);
        sensor.humidity = getRandom(0, initialize.exp_hum * 0.5);
    }
    console.dir(sensor);
    res.send(new Buffer('OK'));
})


app.listen(3000, function(){
    console.log('server up on porta 3000');
});
