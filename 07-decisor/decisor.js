var MongoClient = require('mongodb').MongoClient;
var calculateConvexHull = require('geo-convex-hull');
const geolib = require('geolib');
const { TelegramClient } = require('messaging-api-telegram');

// get accessToken from telegram [@BotFather](https://telegram.me/BotFather)
const client = TelegramClient.connect('851350707:AAEaBWvn4j4WTwgG6lc2LqGw5nt8_PVT6bk');


var url = "mongodb://mongodb:27017/";

const period = parseInt(process.env.PERIOD_ENV, 10);
const MAX_TEMP = parseFloat(process.env.MAX_TEMP);
const MAX_HUM = parseFloat(process.env.MAX_HUM);
let bot = 0;
const flame = "\u{1F525}";


var getelement = function(index){
    return new Promise(function (resolve, reject) { MongoClient.connect(url, function(err, db){

        var dbo = db.db("mydb");

        dbo.collection("collection-"+index).findOne(
            {},
            { sort: { _id: -1 } },
            (err, data) => {
                if(err) reject(err);
                else resolve(data);
            }
        );
        db.close();
    });
    })
};

var dim = function(){
    return new Promise(function (resolve, reject) { MongoClient.connect(url, function(err, db){

        var dbo = db.db("mydb");
        dbo.listCollections().toArray(function(err, collections){
            if(err) reject(err);
            else resolve(collections.length);
        });

        db.close();
    });
    });
};

async function test (){
    let result = [];
    let hot = [];
    let count = 0;
    let area = 0;
    let center;

    let N = await dim();

    if( N > 0)
    {
        for (let i = 0; i < N; i ++) {
            result[i]=await getelement(i);
        }
        for (let i = 0; i < N; i ++) {
            //console.log(result[i]);
            if(result[i]['temp'] > MAX_TEMP && result[i]['hum'] < MAX_HUM) {
                hot[count] = {latitude: result[i]['lat'], longitude: result[i]['lon']};
                count++;
            }
        }
    }

    if(count > 0 && bot == 0)
    {
        console.log(hot);
        var convexHull = calculateConvexHull(hot);
        console.log(convexHull);

        area = geolib.getAreaOfPolygon(convexHull);
        center = geolib.getCenter(convexHull);

        client.sendMessage(29726132, flame+'INCENDIO IN CORSO!'+flame+'\n\n'+'Area: '+area+'\nCentro: '+center['latitude']+', '+center['longitude'], {
            disable_web_page_preview: true,
            disable_notification: true,
        });
        client.sendMessage(279624233, flame+'INCENDIO IN CORSO!'+flame+'\n\n'+'Area: '+area+'\nCentro: '+center['latitude']+', '+center['longitude'], {
            disable_web_page_preview: true,
            disable_notification: true,
        });

        bot = 1;
    }
};

setInterval(test, period);