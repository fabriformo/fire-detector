var express = require('express')
var app = express()

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://mongodb:27017/";

// Max number of elements in capped collections (last 2 hours)
const elements = (2*60*60)/(parseInt(process.env.PERIOD_ENV, 10));

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.listen(3000, function () {
    console.log('ciao')
});

app.post('/', function (req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");

        // Document to be inserted.
        var myobj = req.body;

        // Name collection.
	    var name = 'collection-';
	    name += myobj['id'];


        // Capped collection.
        var capped = dbo.collection(name);


        capped.find().count(function(err, count) {

            if(err) throw err;

            if (count === 0) {
                console.log("Creating collection...");
                dbo.createCollection(name,
                    { "capped": true,
                        "size": 100000,
                        "max": elements },
                    function(err, collection) {
                        if(err) throw err;

                        // Insert a document here.
                        console.log("Inserting document...");
                        collection.insert(myobj, function(err, result) {
                            if (err) throw err;
                        });
                    });

            } else {

                // Insert your document here without creating collection.
                console.log("Inserting document without creating collection...");
                capped.insert(myobj, function(err, result) {
                    if (err) throw err;
                });
            }

        });

        res.json("OK");
    });
});
